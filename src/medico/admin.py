from django.contrib import admin
from .models import Medico, Prevision

# Register your models here.
class AdminPrevision(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']

    class Meta:
        model = Prevision


class AdminMedico(admin.ModelAdmin):
    list_display = ['id', 'nombre', 'apellido_p', 'apellido_m', 'rut']
    search_fields = ['nombre', 'apellido_p', 'apellido_m', 'rut']

    class Meta:
        model = Medico

admin.site.register(Medico, AdminMedico)
admin.site.register(Prevision, AdminPrevision)
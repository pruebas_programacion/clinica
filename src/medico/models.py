from django.db import models

# Create your models here.
from lugares.models import Comuna


class Prevision(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Previsión'
        verbose_name_plural = 'Previsiones'


class Especialidad(models.Model):
    nombre = models.CharField(max_length=255, null=False, blank=False, unique=True)
    descripcion = models.CharField(max_length=255)

    def __str__(self):
        return '{0} {1}'.format(self.nombre, self.descripcion)


class Medico(models.Model):
    rut = models.CharField(max_length=10, unique=True)
    nombre = models.CharField(max_length=100, null=False, blank=False)
    apellido_p = models.CharField(max_length=50, null=False, blank=False)
    apellido_m = models.CharField(max_length=50, default='S/A')
    fecha_nacimiento = models.DateField(auto_now_add=False, auto_now=False)
    fono = models.CharField(verbose_name="telefono", max_length=9)
    domicilio = models.CharField(max_length=100)
    ciudad = models.ForeignKey(Comuna)
    especialidades = models.CharField(max_length=500, null=False, blank=False, default='Medicina General')
    prevision = models.ManyToManyField(Prevision)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Médico'
        verbose_name_plural = 'Médicos'

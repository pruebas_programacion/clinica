from django import forms

from utils.forms import UtilForm
from .models import Paciente


class PacienteForm(UtilForm):
    autofocus_field = 'nombre'

    class Meta:
        model = Paciente
        fields = '__all__'
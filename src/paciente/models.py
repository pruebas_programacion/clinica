from django.db import models

from medico.models import Prevision
from . import manager

# Create your models here.
class Paciente(models.Model):
    rut = models.CharField(max_length=10, unique=True)
    nombre = models.CharField(max_length=100, null=False, blank=False)
    apellido_p = models.CharField(max_length=50, null=False, blank=False)
    apellido_m = models.CharField(max_length=50, default='S/A')
    fecha_nacimiento = models.DateField(auto_now_add=False, auto_now=False)
    fono = models.CharField(max_length=9)
    domicilio = models.CharField(max_length=100)
    ciudad = models.CharField(max_length=100)
    prevision = models.ForeignKey(Prevision)

    objects = manager.PacienteManager()

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Paciente'
        verbose_name_plural = 'Pacientes'



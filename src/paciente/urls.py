from django.conf.urls import url

from . import views


urlpatterns = [

    url(r'^paciente/create/$', views.CreateView.as_view(), name='paciente_create'),
    url(r'^paciente/update/(?P<pk>[0-9]+)/$', views.UpdateView.as_view(), name='paciente_update'),
    url(r'^paciente/delete/(?P<pk>[0-9]+)/$', views.DeleteView.as_view(), name='paciente_delete'),
    url(r'^paciente/$', views.ListView.as_view(), name='paciente_list'),

]

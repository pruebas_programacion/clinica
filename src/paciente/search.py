from django_filters import FilterSet
from .models import Paciente


class SearchRegionForm(FilterSet):
    """Formulario de busqueda de Pacientes."""

    class Meta:
        model = Paciente
        fields = {
            'rut': ['icontains'],
        }
from django.db.models import Manager, Q

class PacienteManager(Manager):
    def find(self, search):
        if not search:
            return self

        qs = Q(rut__icontains=search) | Q(nombre__icontains=search) | Q(apellido_p__icontains=search)

        return self.filter(qs)
from django.urls import reverse_lazy

from utils.views import UtilListView, UtilDeleteView, UtilCreateView, UtilUpdateView
from django_filters.views import FilterView

from . import forms, models, tables, search


class ListView(FilterView, UtilListView):
    template_name = 'paciente/list.html'
    table_class = tables.PacienteTable
    paginate_by = 20
    model_class = models.Paciente
    required_permissions = ('paciente.view_paciente',)
    change_permission = 'paciente.change_paciente'
    delete_permission = 'paciente.delete_paciente'
    filterset_class = search.SearchRegionForm


class CreateView(UtilCreateView):
    template_name = 'paciente/create.html'
    model = models.Paciente
    form_class = forms.PacienteForm
    success_url = reverse_lazy('paciente:paciente_list')
    success_message = 'Paciente creada exitosamente'
    required_permissions = ('paciente.add_paciente',)


class UpdateView(UtilUpdateView):
    template_name = 'paciente/update.html'
    model = models.Paciente
    form_class = forms.PacienteForm
    success_url = reverse_lazy('paciente:paciente_list')
    success_message = 'Paciente actualizada exitosamente.'
    required_permissions = ('paciente.change_region',)


class DeleteView(UtilDeleteView):
    template_name = 'paciente/confirm_delete.html'
    model = models.Paciente
    success_url = reverse_lazy('paciente:paciente_list')
    success_message = 'Paciente eliminada exitosamente'
    required_permissions = ('paciente.delete_region',)

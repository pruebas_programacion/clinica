from django.contrib import admin
from .models import Paciente

class AdminPaciente(admin.ModelAdmin):
    list_display = ['id', 'nombre', 'apellido_p', 'apellido_m', 'rut']
    search_fields = ['nombre', 'apellido_p', 'apellido_m', 'rut']

    class Meta:
        model = Paciente

# Register your models here.
admin.site.register(Paciente, AdminPaciente)


from django.urls import reverse_lazy
from utils.tables import UtilTable
from .models import Paciente


class PacienteTable(UtilTable):
    class Meta:
        model = Paciente
        template = 'django_tables2/bootstrap-responsive.html'
        sequence = (
            'id',
            'rut',
            'nombre',
            'apellido_p',
            'apellido_m',
            'fecha_nacimiento',
            'fono',
            'domicilio',
            'ciudad',
            'prevision',
        )

    def reverse_update(self, record):
        return reverse_lazy('paciente:paciente_update', kwargs={'pk': record.pk})

    def reverse_delete(self, record):
        return reverse_lazy('paciente:paciente_delete', kwargs={'pk': record.pk})
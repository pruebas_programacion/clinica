from django.db import models
from medico.models import Medico
from paciente.models import Paciente

# Create your models here.
class FichaPaciente(models.Model):
    medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fecha_atencion = models.DateField(auto_now_add=True, auto_now=False)
    motivo_consulta = models.CharField(max_length=500, null=False, default='Control Anual')
    diagnostico = models.CharField(max_length=500, null=False, blank=False)
    tratamiento = models.CharField(max_length=500)
    fecha_proximo_control = models.DateField(verbose_name="fecha próximo control",auto_now_add=False, auto_now=False)

    def __str__(self):
        return '{0} ({1})'.format(self.paciente.nombre, self.fecha_atencion)

    class Meta:
        verbose_name = 'Ficha del Paciente'
        verbose_name_plural = 'Ficha del Paciente'




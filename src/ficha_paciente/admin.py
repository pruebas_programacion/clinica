from django.contrib import admin
from .models import FichaPaciente

class AdminFichaPaciente(admin.ModelAdmin):
    list_display = ['paciente', 'medico', 'diagnostico']

    class Meta:
        Model = FichaPaciente



# Register your models here.
admin.site.register(FichaPaciente, AdminFichaPaciente)

from django.apps import AppConfig


class FichaPacienteConfig(AppConfig):
    name = 'ficha_paciente'

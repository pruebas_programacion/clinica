import os
import json

from .models import Region, Comuna

def _get_json(filename):
    return json.load(open(os.path.join(os.path.dirname(__file__), 'data/' + filename)))


def run_regiones_comunas():
    regiones = _get_json('regiones.json')

    for region in regiones['RECORDS']:
        Region.objects.get_or_create(
            nombre=region['region']
        )
        for comuna in region['comunas']:
            Comuna.objects.get_or_create(
                nombre=comuna
            )


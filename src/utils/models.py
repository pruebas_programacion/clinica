from django.db import models

from reversion import register
from reversion.admin import VersionAdmin
from reversion.models import Version


@register()
class ReversionModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creado el')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Actualizado el')

    class Meta:
        abstract = True

    def get_versions(self):
        return Version.objects.get_for_object(self)


class AdminBaseModel(VersionAdmin):
    change_list_template = None
    exclude = ['created_at']


def get_plural_name(model):
    return model._meta.verbose_name_plural.title()


def get_singular_name(model):
    return model._meta.verbose_name.title()
